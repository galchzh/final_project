<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.13.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  '2amigos/yii2-selectize-widget' => 
  array (
    'name' => '2amigos/yii2-selectize-widget',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@dosamigos/selectize' => $vendorDir . '/2amigos/yii2-selectize-widget/src',
    ),
  ),
  '2amigos/yii2-taggable-behavior' => 
  array (
    'name' => '2amigos/yii2-taggable-behavior',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@dosamigos/taggable' => $vendorDir . '/2amigos/yii2-taggable-behavior/src',
    ),
  ),
  'dergus/yii2-whitewizard' => 
  array (
    'name' => 'dergus/yii2-whitewizard',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@dergus/whitewizard' => $vendorDir . '/dergus/yii2-whitewizard',
    ),
  ),
  'drsdre/yii2-wizardwidget' => 
  array (
    'name' => 'drsdre/yii2-wizardwidget',
    'version' => '1.2.3.0',
    'alias' => 
    array (
      '@drsdre/wizardwidget' => $vendorDir . '/drsdre/yii2-wizardwidget',
    ),
  ),
  'hauntd/yii2-vote' => 
  array (
    'name' => 'hauntd/yii2-vote',
    'version' => '0.3.2.0',
    'alias' => 
    array (
      '@hauntd/vote' => $vendorDir . '/hauntd/yii2-vote',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.9.4.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base/src',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '1.5.5.0',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform/src',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog/src',
    ),
  ),
  'kartik-v/yii2-detail-view' => 
  array (
    'name' => 'kartik-v/yii2-detail-view',
    'version' => '1.7.9.0',
    'alias' => 
    array (
      '@kartik/detail' => $vendorDir . '/kartik-v/yii2-detail-view/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'kartik-v/yii2-icons' => 
  array (
    'name' => 'kartik-v/yii2-icons',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/icons' => $vendorDir . '/kartik-v/yii2-icons/src',
    ),
  ),
  'kartik-v/yii2-social' => 
  array (
    'name' => 'kartik-v/yii2-social',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/social' => $vendorDir . '/kartik-v/yii2-social',
    ),
  ),
  'kartik-v/yii2-widget-rating' => 
  array (
    'name' => 'kartik-v/yii2-widget-rating',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@kartik/rating' => $vendorDir . '/kartik-v/yii2-widget-rating/src',
    ),
  ),
  'kartik-v/yii2-widget-typeahead' => 
  array (
    'name' => 'kartik-v/yii2-widget-typeahead',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@kartik/typeahead' => $vendorDir . '/kartik-v/yii2-widget-typeahead/src',
    ),
  ),
  'paulzi/yii2-sortable' => 
  array (
    'name' => 'paulzi/yii2-sortable',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@paulzi/sortable' => $vendorDir . '/paulzi/yii2-sortable',
    ),
  ),
  'yii2mod/yii2-enum' => 
  array (
    'name' => 'yii2mod/yii2-enum',
    'version' => '1.7.1.0',
    'alias' => 
    array (
      '@yii2mod/enum' => $vendorDir . '/yii2mod/yii2-enum',
    ),
  ),
  'yii2mod/yii2-moderation' => 
  array (
    'name' => 'yii2mod/yii2-moderation',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@yii2mod/moderation' => $vendorDir . '/yii2mod/yii2-moderation',
    ),
  ),
  'yii2mod/yii2-editable' => 
  array (
    'name' => 'yii2mod/yii2-editable',
    'version' => '1.5.0.0',
    'alias' => 
    array (
      '@yii2mod/editable' => $vendorDir . '/yii2mod/yii2-editable',
    ),
  ),
  'yii2mod/yii2-behaviors' => 
  array (
    'name' => 'yii2mod/yii2-behaviors',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@yii2mod/behaviors' => $vendorDir . '/yii2mod/yii2-behaviors',
    ),
  ),
  'paulzi/yii2-adjacency-list' => 
  array (
    'name' => 'paulzi/yii2-adjacency-list',
    'version' => '2.2.0.0',
    'alias' => 
    array (
      '@paulzi/adjacencyList' => $vendorDir . '/paulzi/yii2-adjacency-list',
    ),
  ),
  'asofter/yii2-imperavi-redactor' => 
  array (
    'name' => 'asofter/yii2-imperavi-redactor',
    'version' => '0.0.3.0',
    'alias' => 
    array (
      '@yii/imperavi' => $vendorDir . '/asofter/yii2-imperavi-redactor/yii/imperavi',
    ),
  ),
  'yii2mod/yii2-comments' => 
  array (
    'name' => 'yii2mod/yii2-comments',
    'version' => '1.9.9.2',
    'alias' => 
    array (
      '@yii2mod/comments' => $vendorDir . '/yii2mod/yii2-comments',
    ),
  ),
  'yii2mod/yii2-star-rating' => 
  array (
    'name' => 'yii2mod/yii2-star-rating',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@yii2mod/rating' => $vendorDir . '/yii2mod/yii2-star-rating',
    ),
  ),
);
